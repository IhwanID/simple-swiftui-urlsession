//
//  ContentView.swift
//  Tourism
//
//  Created by Ihwan ID on 15/11/20.
//

import SwiftUI
import URLImage

struct ContentView: View {
    @State var places = [Place]()

    var body: some View {
        NavigationView{
            List(places, id: \.id){ item in
                VStack(alignment: .center){
                    URLImage(url: URL(string: item.image)!,
                             content: { image in
                                 image
                                     .resizable()
                                     .aspectRatio(contentMode: .fit)
                             })
                    Text(item.name)
                    NavigationLink(destination: DetailView()){
                        EmptyView()
                    }.frame(width: 0)
                    .opacity(0)
                }

            }.onAppear(perform: {
                URLSession.shared.dataTask(with: URLRequest(url: URL(string: "https://tourism-api.dicoding.dev/list")!)){ data, response, error in
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(PlacesResponse.self, from: data){
                            DispatchQueue.main.async{
                                self.places = decodedResponse.places
                            }
                            return
                        }

                    }
                }.resume()

            }).navigationTitle("Tourism Place")
            .navigationBarTitleDisplayMode(.inline)
        }
    }


}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct DetailView: View{

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View{
        VStack{
            Text("Hello")
        }.navigationBarTitleDisplayMode(.inline)
        .navigationBarBackButtonHidden(true)
        .toolbar {
            ToolbarItem(placement: .principal) {
                HStack {
                    Text("Title")
                        .font(.headline)
                }
            }
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }){
                    HStack {
                        Image(systemName: "chevron.left").foregroundColor(.orange)
                        Text("Back")
                            .font(.headline)
                            .foregroundColor(.orange)
                    }
                }
            }
        }
    }
}
struct PlacesResponse: Decodable {

  let places: [Place]

}

struct Place: Decodable {

    let id: Int
    let name: String
    let description: String
    let address: String
    let longitude: Double
    let latitude: Double
    let like: Int
    let image: String

}

