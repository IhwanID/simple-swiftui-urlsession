//
//  TourismApp.swift
//  Tourism
//
//  Created by Ihwan ID on 15/11/20.
//

import SwiftUI

@main
struct TourismApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
